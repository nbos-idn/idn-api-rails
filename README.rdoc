=== Idn Api Rails Server
This application is a part of NBOS Network developed by Wavelabs Team. This application have ready to use To-Do module API. Here is the {link}[http://nbos.in/docs/module/module.html] to know more about NBOS Modules.

== Requirements
* Ruby 2.2.3
* Rails 4.2.4
* Mysql

== Setup
* Clone the repository & install required gems
	<tt>$>git clone https://gitlab.com/wavelabs/idn-api-rails.git</tt>
	<tt>$>cd idn-api-rails</tt>
	<tt>$>bundle install</tt>
* Database Configuration
	After successfull installation of all dependencies, Open config/database.yml & update with your local mysql credintials. 
	<tt>$>rake db:setup</tt>
	<tt>$>rails server</tt>

== To-Do Module Api Endpoints
* http://localhost:3000/api/todo/v0/todo

== How to develop module?
