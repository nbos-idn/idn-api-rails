# Idn Api Rails Server

This application is a part of NBOS Network developed by Wavelabs Team. This application have ready to use To-Do module API. Here is the [link](http://nbos.in/docs/module/module.html) to know more about NBOS Modules. 

## Requirements
* Docker

## Setup

  Step1. Install Docker if not installed
  https://docs.docker.com/engine/installation/
  
  Step2. Clone the repository & install required gems
  ```shell
  $>git clone https://gitlab.com/wavelabs/idn-api-rails.git
  $>cd idn-api-rails
  $>docker-compose build
  $>docker-compose up
  ```
  
  Step3. Database Configuration, After successful installation of all dependencies, Open config/database.yml & update with your local mysql credentials. And run the following commands.
  ```shell
  $>docker-compose run --rm app rake db:setup
  ```
  
  It will create the database schema & users, tenants and todos tables.
  
  Step4. If you don't want to use cache skip this step, otherwise install & run the Redis server. uncomment the Redis Config section in config/application.yml file.

  Step5. Now Your Rails Todo Module API server is ready.

## To-DO Module API

  * To interact with NBOS IDN API Server we need client credentials(client_key & client_secret) & module credentials(client_key, client_secret, module_key & scope of the token verify).
  * To get client credentials first register [here](http://console.nbos.io) & create a project ge the client_key, client_secret. 
  * To get module client credentials register [here](http://dev.nbos.io) & create a module get the client_key, client_secret & module_key.
  * Admin will approve the module which you have created.
  * Once your module get approved you can find that module in project dash board marketplace where you have created your project.
  * You can subscribe that module to your project. Now you are ready.
  * We have already those details & configured in config/idn_config.yml file. 
  * Following are ready to use APIs of To-Do module.

  URL                                          | Method | Authorization | Body(JSON format)
  -------------------------------------------- | ------ | -----------   | ----------------
  htpp://localhost:3000/api/todo/v0/todos     | GET    | Client Token  |  NA 
  htpp://localhost:3000n/api/todo/v0/todos     | GET    | User Token    |  NA
  htpp://localhost:3000/api/todo/v0/todos     | POST   | User Token    | {"title": "mytodo", "note": "This is my first todo", "completed": false, "public": false}
  htpp://localhost:3000/api/todo/v0/todos/:id | GET    | User Token    |  NA
  htpp://localhost:3000/api/todo/v0/todos/:id | PUT    | User Token    | {"title": "mytodo", "note": "This is my first todo", "completed": true}
    

## Development

1. Setup a new Module [here](http://dev.nbos.io) using sample user credentials & get the module client_key, client_secret & module_key.

2. Add the module credentials in config/idn_config.yml file modules section as follows. For example if the module name is 'event'
    ```yml
      event:
        host: "host name"
        client_key: "client key"
        client_secret: "client secret"
        module_key: "module key"
        scope: "scope of the the token"
    ```
  Note: Please carefull about the indentation(only spaces) while adding module credentials. Because the configuration file is in yml format.

3. Create API endpoints(routes) for your module.

      For example if you want to create an event module. For that follow the naming convention & Rails routing syntax
      ```ruby
      namespace :api , path: 'api' do
        namespace :com , path: nil do 
          namespace :nbos, path: nil do
            namespace :modules, path: nil do
              namespace :event, path: 'event' do
                namespace :v0, path: 'v0' do
                  get '/events' => "event#index"
                  #Add your module specific routes
                end
              end
            end
          end 
        end
      end
      ```

     Above route will create API endpoint "/api/event/v0/events" with GET request. You can add whatever endpoints required for your module after "get '/events' => "event#index"" line.

4. Create required models(tables) & associations.

      Following is the directory structure to add new module models(tables).

      ![alt text](https://gitlab.com/wavelabs/idn-api-rails/raw/master/documentaion/images/models_dir_structure.png "Models Directory Structure")

      a. Create the models & migrations.

      b. Add required columns to tables.

      c. Add validations to models based on columns.

      d. Add associations to models if your module requires more than one model.

      f. Associate users & tenant tables with your core module table. That means you need add user_id & tenant_id to core model of your module.

      Note: Add your modules related models under app/models/com/nbos/modules/your_module_name/v0/

5. Create your controller & implement the response logic to your endpoints.

      Following is the directory structure to add new module models(tables).

      ![alt text](https://gitlab.com/wavelabs/idn-api-rails/raw/master/documentaion/images/controllers_dir_structure.png "Controller Directory Structure")

      a. Create a controller & add one before filter.

      b. Add required logic to your API specific actions.

      c. Now the controller will look like this.
      ```ruby
        class Api::Com::Nbos::Modules::Event::V0::EventsController < ApplicationController
          respond_to :json, :xml

          before_action do
            module_verify("event")
          end

          #Get /events
          def index
            #Application logic
          end
        end
      ```
6. Response Error handling & Error messages rules.

    Response Status Code | Description          | Message(JSON format) 
    ---------------------| ---------------------| --------------------
      400(Client Error)  | Bad Request          | {messageCode: "module.badrequest", message: "Bad Request"}        
      401(Client Error)  | Unauthorized         | {messageCode: "module.unauthorized", message: "Unauthorized"}
      404(Client Error)  | Not Found            | {messageCode: "module.endpoint", message: "Endpoint Not Found"}
      500(Server Error)  | Internal Server Error| {messageCode: "internal.server.error", message: "Internal Server Error"}
      500(Server Error)  | Validation Error     | { "errors": [{"messageCode": "todo.title", "message": "title empty","propertyName": "title","objectName": ""},{"messageCode": "todo.note","message": "note empty","propertyName": "note","objectName": ""}]}

      a. The response message should contain 'messageCode' & 'message'

      b. 'messageCode' value should follow the naming like 'module.badrequest', because in future we will give support for multi language support.

      c. 'message' value should contain meaningfull status message.

      d. If your tables/models have validations we need to handle those too. We are considering that as 500 error(last column of above table). To generate such error messages we have implemented a method 'add_error_messages(obj)' in 'application_controller'. You can use that method in your controller as follows.
      ```ruby
        def create
          ......
          .......
            if model.save
              render :json => {messageCode: "success", message: "Todo Created Successfully"}
            else
              data = add_error_messages(model)
              render :json => data
            end
          .....
        end
      ```
      Note: 'add_error_messages(obj)' method will generate error message for a model. If you want to validate associated tables/models you need to impletement your own method, but the format should use the following structure.
      ```ruby
        {
          "errors": [
            {
              "messageCode": "todo.title",
              "message": "title empty",
              "propertyName": "title",
              "objectName": ""
            },
            {
              "messageCode": "todo.note",
              "message": "note empty",
              "propertyName": "note",
              "objectName": ""
            }
          ]
        }
      ```

## Contributing

Bug reports and pull requests are welcome on GitHub at https://gitlab.com/[USERNAME]/idn_sdk_ruby. This project is intended to be a safe, welcoming space for collaboration, and contributors are expected to adhere to the [Contributor Covenant](contributor-covenant.org) code of conduct.


## License

The gem is available as open source under the terms of the [MIT License](http://opensource.org/licenses/MIT).

