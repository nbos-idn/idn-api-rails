class Api::Com::Nbos::Modules::Todo::V0::TodoController < ApplicationController

	# Before action to verify the token subscription & validation
	before_action do
		# module Name should be identical with the name of module
		# in dev.nbos.in & also maintain the same name in config/idn_config.yml
		# file while configuring module details
		module_verify("RoR-Todo")
	end	

	# This method will return token based todos
	# If the token is user token it will return user's todos
	# If the token is client token it will return it's tenant public todos 
	def index
		if @user.uuid.present?
			@todos = @user.todos
		elsif @user.username == "guest"
			@todos = ::Com::Nbos::Modules::Todos::Todo.where(public: true, tenant_id: @user.tenant.idn_tenant_id)
		end
		render json: @todos	
	end
		
	# This method will create a todo & associate with the user
	def create
		if params[:todo].present? && @user.uuid.present?
			todo = ::Com::Nbos::Modules::Todos::Todo.new(params[:todo].permit!)
			todo.user_id = @user.id
			todo.tenant_id = @user.tenant.idn_tenant_id
			if todo.save
				render :json => {messageCode: "success", message: "Todo Created Successfully"}
			else
				data = add_error_messages(todo)
				render :json => data
			end
		else
			render :json => {messageCode: "bad.request", message: "Bad Request"}, status: 400
		end	
	end

	# This method will show the todo based on the requested todo ID
	def show
		if params[:id].present?
			@todo = ::Com::Nbos::Modules::Todos::Todo.where(id: params[:id], tenant_id: @module_token_details.tenantId).first
			if @todo.present? && @todo.public
				render json: @todo
			elsif @user.username != "guest" && @todo && @todo.user_id == @user.id
				render json: @todo
			else
				render :json => {messageCode: "module.user.unauthorized", message: "Unauthorized to view others TODO"}, status: 404
			end	
		else
			render :json => {messageCode: "bad.request", message: "Bad Request"}, status: 400
		end	
	end

	# This Method will update the todo based on requested todo ID
	# by validating the user token. That means only owner of the todo
	# can update.
	def update
		if params[:id].present? && params[:todo].present? 
			@todo = @user.todos.where(id: params[:id]).first
			if @todo.present? 
				@todo.update_columns(params[:todo].permit!)
				if @todo.save
					render :json => @todo
				else
					data = add_error_messages(@todo)
					render :json => data
				end
			else
				render :json => {"messageCode": "module.user.unauthorized", "message": "Unauthorized to update others TODO"}, status: 404
			end
		else
			render :json => {messageCode: "bad.request", message: "Bad Request"}, status: 400
		end	

	end

	# This Method will delete the todo based on requested todo ID
	# by validating the user token. That means only user who have 
	# admin authorities can delete.
	def delete
		if params[:id].present?
			if @module_token_details.get_authorities.include?("authority:core.module.admin")
				@todo = ::Com::Nbos::Modules::Todos::Todo.where(id: params[:id], tenant_id: @module_token_details.tenantId).first
				if @todo.present? && @todo.destroy
					render :json => {messageCode: "module.delete.success", message: "Todo deleted successfully."}, status: 200
				else
					render :json => {messageCode: "module.not.found", message: "Todo Not found"}, status: 404
				end	
			else
				render :json => {messageCode: "module.unauthorized", message: "You don't have authority to delete"}, status: 401
			end	
		else
			render :json => {messageCode: "bad.request", message: "Bad Request"}, status: 400
		end	
	end	
	
end
