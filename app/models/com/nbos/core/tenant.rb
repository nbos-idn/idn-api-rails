# This is the core Tenant modle which will store IDN tenant related info.
# Module developer needs to associated this model with his/her module's models  
class Com::Nbos::Core::Tenant < ActiveRecord::Base
	has_many :users, class_name: "Com::Nbos::Core::User"
end
