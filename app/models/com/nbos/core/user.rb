# This is the core user modle which will store the idn user related info.
# Module developer needs to associated this model with his/her module's models 
class Com::Nbos::Core::User < ActiveRecord::Base
	belongs_to :tenant, class_name: "Com::Nbos::Core::Tenant"
	has_many :todos, class_name: "Com::Nbos::Modules::Todos::Todo"
end
