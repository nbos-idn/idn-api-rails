module Com
	module Nbos
		module Modules
			module Todos
				class Todo < ActiveRecord::Base
					belongs_to :user, class_name: "Com::Nbos::Core::User"

					validates :title, :note, presence: true
						
				end
			end
		end
	end
end				
