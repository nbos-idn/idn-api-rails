class CreateComNbosCoreUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
    	t.string :username
    	t.string :uuid
    	t.integer :tenant_id
      t.timestamps null: false
    end
  end
end
