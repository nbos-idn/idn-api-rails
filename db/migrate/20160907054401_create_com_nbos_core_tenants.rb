class CreateComNbosCoreTenants < ActiveRecord::Migration
  def change
    create_table :tenants do |t|
    	t.string :name
    	t.string :idn_tenant_id
      t.timestamps null: false
    end
  end
end
