class CreateComNbosModulesTodosTodos < ActiveRecord::Migration
  def change
    create_table :todos do |t|
    	t.string :title
    	t.text :note
    	t.boolean :completed, default: false
    	t.boolean :public, default: false
    	t.integer :user_id
    	t.string :tenant_id
      t.timestamps null: false
    end
  end
end
