FROM ruby:2.2.3
MAINTAINER Jagadishwer Bathula
RUN apt-get update -qq && apt-get install -y build-essential libpq-dev nodejs
RUN mkdir /idn-api-rails
WORKDIR /idn-api-rails
ADD Gemfile /idn-api-rails/Gemfile
ADD Gemfile.lock /idn-api-rails/Gemfile.lock
RUN bundle install
ADD . /idn-api-rails
